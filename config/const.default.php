<?php
use Cake\Core\Configure;

return [
    Configure::write(
        [
            'DATABASE_USERNAME' => 'root',
            'DATABASE_PASSWORD' => 'root'
        ]
    ),
];