<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;

/**
 * Arithmetic component
 */
class ArithmeticComponent extends Component
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    /**
     * add method
     * This function will add two given parameters
     *
     * It will return error message if parameter is not correct
     * It will return 0 value if paramter is empty
     *
     * @param int $num1
     * @param int $num2
     * @return string error message
     * @return int sum result
     */
    public function add(
        $num1 = 0,
        $num2 = 0
    ) {
        if (!is_numeric($num1)) {
            return 'First parameter should be numeric';
        } else if (!is_numeric($num2)) {
            return 'Second parameter should be numeric';
        } else if (!is_numeric($num1) && !is_numeric($num2)) {
            return 'All parameter should be numeric';
        }

        return $num1 + $num2;
    }
}
